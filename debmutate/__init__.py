#!/usr/bin/python

"""Make changes to Debian packages."""

__version__ = (0, 68)
version_string = '.'.join([str(x) for x in __version__])
